Feature: Get Account Permissions With AccountId
	Get Account Permissions with accountId
	
   Scenario: Success
    Given I provide a valid accessToken identifying me as <identity>
    When I GET to /accountId
    Then AccountPermissionsSynopsisView is returned from database