package com.precorconnect.accountpermissionsservice.sdk;

import java.net.MalformedURLException;
import java.net.URL;

public class AccountPermissionsServiceSdkConfigFactoryImpl implements AccountPermissionsServiceSdkConfigFactory {

	@Override
	public AccountPermissionsServiceSdkConfig construct() {
		
		return
				new   AccountPermissionsServiceSdkConfigImpl(
				    			constructprecorConnectApiBaseUrl()
				    	);
	}
	
	
	public URL constructprecorConnectApiBaseUrl(){

		try {

            String precorConnectApiBaseUrl=System
            		            .getenv("PRECOR_CONNECT_API_BASE_URL");

            return
            		new URL(
            		  precorConnectApiBaseUrl
            		);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }
}

}
