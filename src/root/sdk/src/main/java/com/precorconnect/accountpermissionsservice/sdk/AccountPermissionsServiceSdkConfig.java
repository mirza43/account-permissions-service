package com.precorconnect.accountpermissionsservice.sdk;

import java.net.URL;

public interface AccountPermissionsServiceSdkConfig {
	
	URL getPrecorConnectApiBaseUrl();

}
