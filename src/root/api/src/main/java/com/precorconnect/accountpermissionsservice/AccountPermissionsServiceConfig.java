package com.precorconnect.accountpermissionsservice;

import com.precorconnect.accountpermissionsservice.database.DatabaseAdapterConfig;
import com.precorconnect.accountpermissionsservice.identityprovider.IdentityServiceAdapterConfig;

public interface AccountPermissionsServiceConfig {
	
	 DatabaseAdapterConfig getDatabaseAdapterConfig();

	    IdentityServiceAdapterConfig getIdentityServiceAdapterConfig();

}
