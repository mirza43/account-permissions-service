package com.precorconnect.accountpermissionsservice.webapi.accountpermissionssynopsisview;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Assert;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.accountpermissionsservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.accountpermissionsservice.webapi.Config;
import com.precorconnect.accountpermissionsservice.webapi.ConfigFactory;
import com.precorconnect.accountpermissionsservice.webapi.Dummy;
import com.precorconnect.accountpermissionsservice.webapi.Factory;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;




public class StepDefs extends AbstractSpringIntegrationTest {
	
	private final Config config =
            new ConfigFactory().construct();

    private final Dummy dummy = new Dummy();

    private final Factory factory =
            new Factory(
                    dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                            config.getIdentityServiceJwtSigningKey()
                    )
            );
    
    private OAuth2AccessToken accessToken;
    
    private Response response;
    
    @Before
    public void beforeAll() {

        RestAssured.port = getPort();

    }
    
    @Given("^I provide a valid accessToken identifying me as <identity>$")
	public void i_provide_a_valid_accessToken_identifying_me_as_identity() throws Throwable {
		accessToken = new OAuth2AccessTokenImpl(
				factory
				.constructValidAppOAuth2AccessToken()
				.getValue()
			);
	}
    
    @When("^I GET to /accountId$")
	public void i_GET_to_accountId() throws Throwable {
	   
		response = given()
		        .contentType(ContentType.JSON)
		        .header(
		                "Authorization",
		                String.format(
		                        "Bearer %s",
		                        accessToken
		                                .getValue()
		                )
		        )
		        .get("/account-permissions/"+dummy.getAccountId().getValue());
                
	}
    
    @Then("^AccountPermissionsSynopsisView is returned from database$")
	public void account_permissions_synopsis_view_will_be_returned_from_database() throws Throwable {
	    
		response
		.then()
		.assertThat()
		.statusCode(200);

	 Assert.assertNotNull(response);
	}

}
