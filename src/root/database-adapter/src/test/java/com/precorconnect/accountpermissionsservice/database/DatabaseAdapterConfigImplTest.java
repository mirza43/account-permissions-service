package com.precorconnect.accountpermissionsservice.database;

import com.precorconnect.Password;
import com.precorconnect.Username;
import org.junit.Test;

import java.net.URI;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.junit.Assert.assertSame;

public class DatabaseAdapterConfigImplTest {

    /*
    fields
     */
    private final Dummy dummy = new Dummy();
    
    /*
    test methods
     */
    @Test(expected = IllegalArgumentException.class)
    public void constructor_UriNull_Throws(
    ) throws Exception {

        new DatabaseAdapterConfigImpl(
                null,
                dummy.getUsername(),
                dummy.getPassword()
        );

    }

    @Test
    public void constructor_SetsUri(
    ) throws Exception {
        
        /*
        arrange
         */

        URI expectedUri = dummy.getUri();

        DatabaseAdapterConfig objectUnderTest =
                new DatabaseAdapterConfigImpl(
                        expectedUri,
                        dummy.getUsername(),
                        dummy.getPassword()
                );
        
        
        /*
        act
         */

        URI actualUri =
                objectUnderTest.getUri();
        
        /*
        assert
         */

        assertSame(expectedUri, actualUri);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_UsernameNull_Throws(
    ) throws Exception {

        new DatabaseAdapterConfigImpl(
                dummy.getUri(),
                null,
                dummy.getPassword()
        );

    }

    @Test
    public void constructor_SetsUsername(
    ) throws Exception {
        
        /*
        arrange
         */

        Username expectedUsername = dummy.getUsername();

        DatabaseAdapterConfig objectUnderTest =
                new DatabaseAdapterConfigImpl(
                        dummy.getUri(),
                        expectedUsername,
                        dummy.getPassword()
                );
        
        
        /*
        act
         */

        Username actualUsername =
                objectUnderTest.getUsername();
        
        /*
        assert
         */

        assertThat(expectedUsername)
                .isSameAs(actualUsername);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_PasswordNull_Throws(
    ) throws Exception {

        new DatabaseAdapterConfigImpl(
                dummy.getUri(),
                dummy.getUsername(),
                null
        );

    }

    @Test
    public void constructor_SetsPassword(
    ) throws Exception {
        
        /*
        arrange
         */

        Password expectedPassword = dummy.getPassword();

        DatabaseAdapterConfig objectUnderTest =
                new DatabaseAdapterConfigImpl(
                        dummy.getUri(),
                        dummy.getUsername(),
                        expectedPassword
                );
        
        
        /*
        act
         */

        Password actualPassword =
                objectUnderTest.getPassword();
        
        /*
        assert
         */

        assertThat(expectedPassword)
                .isSameAs(actualPassword);
    }

}