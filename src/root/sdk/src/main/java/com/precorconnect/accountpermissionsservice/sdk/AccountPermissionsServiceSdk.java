package com.precorconnect.accountpermissionsservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.webapiobjectmodel.AccountPermissionsSynopsisView;

public interface AccountPermissionsServiceSdk {
	
	AccountPermissionsSynopsisView getAccountPermissionWithId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			)throws AuthenticationException;

}
