package com.precorconnect.accountpermissionsservice.objectmodel;

import org.checkerframework.checker.nullness.qual.NonNull;

public class ExtendedWarrantyImpl implements ExtendedWarranty {
	
	/*
	 *  fields
     */
    private final boolean value;

    /*
    constructor methods
     */
    public ExtendedWarrantyImpl(
    		@NonNull boolean value
    		){

    	this.value = value;

    }


	@Override
	public boolean getValue() {
		return value;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (value ? 1231 : 1237);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExtendedWarrantyImpl other = (ExtendedWarrantyImpl) obj;
		if (value != other.value)
			return false;
		return true;
	}
	
	

}
