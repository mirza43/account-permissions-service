package com.precorconnect.accountpermissionsservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;


@Singleton
public class GetAccountPermissionsWithIdFeatureImpl implements GetAccountPermissionsWithIdFeature {
	
	 /*
    fields
     */
    private final SessionFactory sessionFactory;

    private final AccountPermissionsSynopsisViewFactory accountPermissionsSynopsisViewFactory;

    @Inject
    public GetAccountPermissionsWithIdFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final AccountPermissionsSynopsisViewFactory accountPermissionsSynopsisViewFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                        sessionFactory
                		)
                        .isNotNull()
                        .thenGetValue();

    	this.accountPermissionsSynopsisViewFactory =
                guardThat(
                        "accountPermissionsSynopsisViewFactory",
                        accountPermissionsSynopsisViewFactory
                		)
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public AccountPermissionsSynopsisView execute(@NonNull AccountId accountId) {
	
		Session session = null;

        try {
        	 session = sessionFactory.openSession();

             Query query = session.createQuery("from AccountPermissions where accountId = :accountIdValue and  endDate >= :today");

             query.setString("accountIdValue", accountId.getValue());
             
             Calendar cal = Calendar.getInstance();
             Date todaydate = cal.getTime(); 
             
             query.setParameter("today", new InstantPersistenceConverter()
						.convertToEntityAttribute(new Timestamp(todaydate.getTime())));
        	
        	 AccountPermissions accountPermissions = (AccountPermissions) query.uniqueResult();
        	 
        	 if(accountPermissions!=null)
        	 
           return 
        		   accountPermissionsSynopsisViewFactory.
        		   								construct(accountPermissions);
        	 else
        		 return null;
        	
        	 
        } finally {

            if (session != null) {
                session.close();
            }

        }
	}

}
