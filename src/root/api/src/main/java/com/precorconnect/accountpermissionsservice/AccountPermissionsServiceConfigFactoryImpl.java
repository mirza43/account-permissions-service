package com.precorconnect.accountpermissionsservice;

import com.precorconnect.accountpermissionsservice.database.DatabaseAdapterConfigFactoryImpl;
import com.precorconnect.accountpermissionsservice.identityprovider.IdentityServiceAdapterConfigFactoryImpl;

public class AccountPermissionsServiceConfigFactoryImpl implements AccountPermissionsServiceConfigFactory {

	@Override
	public AccountPermissionsServiceConfig construct() {
		
		return 
				new AccountPermissionsServiceConfigImpl(
						new DatabaseAdapterConfigFactoryImpl()
														  .construct(),
						new IdentityServiceAdapterConfigFactoryImpl()
														  .construct()
			   );
	}

}
