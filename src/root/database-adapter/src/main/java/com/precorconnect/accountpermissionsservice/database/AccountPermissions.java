package com.precorconnect.accountpermissionsservice.database;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AccountPermissions")
public class AccountPermissions {
	
	/*
    fields
     */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private String accountId;
    
    private boolean spiff;
    
    private boolean extendedWarranty;
        
    private Instant  startDate;
    
    private Instant endDate;

	public Integer getId() {
		return id;
	}

	public String getAccountId() {
		return accountId;
	}

	public boolean isSpiff() {
		return spiff;
	}

	public boolean isExtendedWarranty() {
		return extendedWarranty;
	}


	public Instant getStartDate() {
		return startDate;
	}

	public Instant getEndDate() {
		return endDate;
	}
	

	public void setId(Integer id) {
		this.id = id;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public void setSpiff(boolean spiff) {
		this.spiff = spiff;
	}

	public void setExtendedWarranty(boolean extendedWarranty) {
		this.extendedWarranty = extendedWarranty;
	}

	
	public void setStartDate(Instant startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Instant endDate) {
		this.endDate = endDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + (extendedWarranty ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (spiff ? 1231 : 1237);
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountPermissions other = (AccountPermissions) obj;
		if (accountId == null) {
			if (other.accountId != null)
				return false;
		} else if (!accountId.equals(other.accountId))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (extendedWarranty != other.extendedWarranty)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (spiff != other.spiff)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}


}
