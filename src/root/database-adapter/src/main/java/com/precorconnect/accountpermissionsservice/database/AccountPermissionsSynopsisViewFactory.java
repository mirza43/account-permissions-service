package com.precorconnect.accountpermissionsservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;

public interface AccountPermissionsSynopsisViewFactory {
	
	AccountPermissionsSynopsisView construct(
			@NonNull AccountPermissions accountPermissions
			);

}
