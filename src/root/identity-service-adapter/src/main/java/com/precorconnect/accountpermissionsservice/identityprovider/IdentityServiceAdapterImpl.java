package com.precorconnect.accountpermissionsservice.identityprovider;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccessContext;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.accountpermissionsservice.IdentityServiceAdapter;

import org.checkerframework.checker.nullness.qual.NonNull;

public class IdentityServiceAdapterImpl implements IdentityServiceAdapter {

	/*
	 * fields
	 */
	private final Injector injector;

	/*
	 * constructors
	 */
	public IdentityServiceAdapterImpl(
			@NonNull IdentityServiceAdapterConfig config
			) {

		GuiceModule guiceModule = new GuiceModule(config);

		injector = Guice.createInjector(guiceModule);
	}

	@Override
	public AccessContext getAccessContext(
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException {

		return 
				injector
				.getInstance(GetAccessContextFeature.class)
				.execute(accessToken);

	}

}
