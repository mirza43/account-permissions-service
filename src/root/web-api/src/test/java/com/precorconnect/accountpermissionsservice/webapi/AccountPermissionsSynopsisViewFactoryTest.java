package com.precorconnect.accountpermissionsservice.webapi;

import static org.assertj.core.api.StrictAssertions.assertThat;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisViewImpl;


public class AccountPermissionsSynopsisViewFactoryTest {
	
	private AccountPermissionsSynopsisViewFactory accountPermissionsSynopsisViewFactory;
	
private final Dummy dummy = new Dummy();	
	
	
	@Before
	public void setUp(){
		accountPermissionsSynopsisViewFactory = new AccountPermissionsSynopsisViewFactoryImpl();
	}
	
	@Test
	public void test_construct_passing_mandatory_fields(){
		
		com.precorconnect.accountpermissionsservice.webapiobjectmodel.AccountPermissionsSynopsisView actualAccountPermissionsViewObj=
				accountPermissionsSynopsisViewFactory.construct(construct_accountPermissionsSynopsisViewObj());
		
		assertThat(actualAccountPermissionsViewObj).isEqualTo(construct_expected_accountPermissionsView_res_obj());
	
	}
	
	
	public com.precorconnect.accountpermissionsservice.webapiobjectmodel.AccountPermissionsSynopsisView construct_expected_accountPermissionsView_res_obj(){
		
		return new com.precorconnect.accountpermissionsservice.webapiobjectmodel.AccountPermissionsSynopsisView(
				
				dummy.getId().getValue(), 
				dummy.getAccountId().getValue(),
				dummy.getSpiff().getValue(),
				dummy.getExtendedWarranty().getValue(),
				
				new SimpleDateFormat("MM/dd/yyyy").format(
						 Date.from(dummy.getStartDate()
								 )
						 )
						 ,
						 
				 new SimpleDateFormat("MM/dd/yyyy").format(
						 Date.from(dummy.
									getStartDate()
						)
					)
						 
				);
	}
	
public AccountPermissionsSynopsisView construct_accountPermissionsSynopsisViewObj(){
		
		return 
				new AccountPermissionsSynopsisViewImpl(dummy.getId(),
						dummy.getAccountId(),
						dummy.getSpiff(),
						dummy.getExtendedWarranty(),
						dummy.getStartDate(),
						dummy.getEndDate()
				);
	}

}
