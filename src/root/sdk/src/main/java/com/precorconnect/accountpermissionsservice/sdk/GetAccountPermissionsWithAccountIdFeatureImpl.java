package com.precorconnect.accountpermissionsservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.webapiobjectmodel.AccountPermissionsSynopsisView;

@Singleton
public class GetAccountPermissionsWithAccountIdFeatureImpl implements GetAccountPermissionsWithAccountIdFeature {
	
	 /*
    fields
     */
    private final WebTarget accountPermissionsWebTarget;
    
    /*
    constructors
     */
    @Inject

	public GetAccountPermissionsWithAccountIdFeatureImpl(
			WebTarget baseWebTarget) {
		this.accountPermissionsWebTarget = 
				guardThat(
                        "baseWebTarget",
                         baseWebTarget
                		)
                        .isNotNull()
                        .thenGetValue().path("account-permissions");
	}




	@Override
	public AccountPermissionsSynopsisView execute(@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken oAuth2AccessToken) throws AuthenticationException {
		
		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        oAuth2AccessToken.getValue()
                );


        try {

        	return
        			accountPermissionsWebTarget
		            		.path("/"+ String.format(
		                            "%s",
		                            accountId.getValue()
		                    ))
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", authorizationHeaderValue)
                            .get(
                            		new GenericType<AccountPermissionsSynopsisView>() {
                    }
                            );

        } catch (NotAuthorizedException e) {

            throw new AuthenticationException(e);

        }

	}

}
