package com.precorconnect.accountpermissionsservice.objectmodel;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;

import com.precorconnect.Password;
import com.precorconnect.PasswordImpl;
import com.precorconnect.Username;
import com.precorconnect.UsernameImpl;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountIdImpl;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsIdImpl;
import com.precorconnect.accountpermissionsservice.objectmodel.ExtendedWarranty;
import com.precorconnect.accountpermissionsservice.objectmodel.ExtendedWarrantyImpl;
import com.precorconnect.accountpermissionsservice.objectmodel.Spiff;
import com.precorconnect.accountpermissionsservice.objectmodel.SpiffImpl;



public class Dummy {

    /*
    fields
     */
   
    
    private final AccountPermissionsId id= 
    		new AccountPermissionsIdImpl(1);
    
    private final AccountId accountId=
    		new AccountIdImpl("161018");
    
    private  final Spiff spiff= 
    				new SpiffImpl(true);
    		
    
    private  final ExtendedWarranty extendedWarranty= 
    			new ExtendedWarrantyImpl(true);
    		
    
    private  final Instant startDate;
    
    private final Instant endDate;

    /*
    constructors
     */
    public Dummy() {

        try {

          
            
            startDate =
		            		new SimpleDateFormat("MM/dd/yyyy").parse(
									 "1/1/2016"
							 	).toInstant();
            
            endDate=
            		new SimpleDateFormat("MM/dd/yyyy").parse(
							 "12/31/2099"
					 	).toInstant();
            				

        } catch ( ParseException e) {

            throw new RuntimeException(e);

        }
    }
    
    /*
    getter methods
     */


	public AccountPermissionsId getId() {
		return id;
	}

	public AccountId getAccountId() {
		return accountId;
	}

	public Spiff getSpiff() {
		return spiff;
	}

	public ExtendedWarranty getExtendedWarranty() {
		return extendedWarranty;
	}

	public Instant getStartDate() {
		return startDate;
	}

	public Instant getEndDate() {
		return endDate;
	}
    

}
