package com.precorconnect.accountpermissionsservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.accountpermissionsservice.database.DatabaseAdapterImpl;
import com.precorconnect.accountpermissionsservice.identityprovider.IdentityServiceAdapterImpl;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;


public class AccountPermissionsServiceImpl implements AccountPermissionsService{
	
	 /*
    fields
     */
    private final Core core;

    /*
    constructors
     */
    public AccountPermissionsServiceImpl(
            @NonNull final AccountPermissionsServiceConfig config
    ) {

        this.core =
                new CoreImpl(
                        new DatabaseAdapterImpl(
                                config.getDatabaseAdapterConfig()
                        ),
                        new IdentityServiceAdapterImpl(
                                config.getIdentityServiceAdapterConfig()
                        )
                );

    }


	@Override
	public AccountPermissionsSynopsisView getAccountPermissionWithId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException {
		
		
		
		return 
				core.getAccountPermissionWithId(
						accountId,
						oAuth2AccessToken
				);
	}

}
