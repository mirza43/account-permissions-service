package com.precorconnect.accountpermissionsservice.sdk;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.identityservice.AppJwt;
import com.precorconnect.identityservice.AppJwtImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdk;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;

import static com.precorconnect.guardclauses.Guards.guardThat;

public class Factory {

    /*
    fields
     */
    private final IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk;

    private final Dummy dummy;

    /*
    constructors
     */
    public Factory(
            @NonNull IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk,
            @NonNull Dummy dummy
    ) {

        this.identityServiceIntegrationTestSdk = 
				guardThat(
						"identityServiceIntegrationTestSdk",
						 identityServiceIntegrationTestSdk
				)
						.isNotNull()
						.thenGetValue();

        this.dummy = 
				guardThat(
						"dummy",
						 dummy
				)
						.isNotNull()
						.thenGetValue();

    }

    public OAuth2AccessToken constructValidAppOAuth2AccessToken() {

        AppJwt appJwt = new AppJwtImpl(
                Instant.now().plusSeconds(480),
                dummy.getUri(),
                dummy.getUri()
        );

        return
                identityServiceIntegrationTestSdk
                        .getAppOAuth2AccessToken(appJwt);
    }

    public AccountPermissionsServiceSdkConfig constructAccountPermissionsServiceSdkConfig(
            Integer port
    ) {

        URL precorConnectApiBaseUrl;

        try {

        	precorConnectApiBaseUrl = new URL(String.format("http://localhost:%s", port));

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

        return
                new AccountPermissionsServiceSdkConfigImpl(
                		precorConnectApiBaseUrl
                );

    }

}
