package com.precorconnect.accountpermissionsservice.objectmodel;

import static org.assertj.core.api.StrictAssertions.assertThat;

import java.time.Instant;

import org.junit.Test;




public class AccountPermissionsSynopsisViewImplTest {
	
	  /*
    fields
     */
    private final Dummy dummy = new Dummy();

    /*
    test methods
     */
    
    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullId_Throws(
    ) throws Exception {

        new AccountPermissionsSynopsisViewImpl(
                null,
                dummy.getAccountId(),
                dummy.getSpiff(),
                dummy.getExtendedWarranty(),
                dummy.getStartDate(),
                dummy.getEndDate()
               
        );

    }
    
    @Test
    public void constructor_SetsId() {
        
        /*
        arrange
         */
    	AccountPermissionsId expectedId = dummy.getId();
        
        /*
        act
         */
        AccountPermissionsSynopsisView objectUnderTest =
                new AccountPermissionsSynopsisViewImpl(
                        expectedId,
                        dummy.getAccountId(),
                        dummy.getSpiff(),
                        dummy.getExtendedWarranty(),
                        dummy.getStartDate(),
                        dummy.getEndDate()
                );
        
        /*
        assert
         */
        AccountPermissionsId actualId =
                objectUnderTest
                        .getId();

        assertThat(actualId)
                .isEqualTo(expectedId);

    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullAccountId_Throws(
    ) throws Exception {

        new AccountPermissionsSynopsisViewImpl(
                dummy.getId(),
                null,
                dummy.getSpiff(),
                dummy.getExtendedWarranty(),
                dummy.getStartDate(),
                dummy.getEndDate()
               
        );

    }
    
    @Test
    public void constructor_SetsAccountId() {
        
        /*
        arrange
         */
    	AccountId expectedAccountId = dummy.getAccountId();
        
        /*
        act
         */
        AccountPermissionsSynopsisView objectUnderTest =
                new AccountPermissionsSynopsisViewImpl(
                        dummy.getId(),
                        expectedAccountId,
                        dummy.getSpiff(),
                        dummy.getExtendedWarranty(),
                        dummy.getStartDate(),
                        dummy.getEndDate()
                );
        
        /*
        assert
         */
        AccountId actualAccountId =
                objectUnderTest
                        .getAccountId();

        assertThat(actualAccountId)
                .isEqualTo(expectedAccountId);

    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullSpiff_Throws(
    ) throws Exception {

        new AccountPermissionsSynopsisViewImpl(
                dummy.getId(),
                dummy.getAccountId(),
                null,
                dummy.getExtendedWarranty(),
                dummy.getStartDate(),
                dummy.getEndDate()
               
        );

    }
    
    @Test
    public void constructor_SetsSpiff() {
        
        /*
        arrange
         */
    	Spiff expectedSpiff = dummy.getSpiff();
        
        /*
        act
         */
        AccountPermissionsSynopsisView objectUnderTest =
                new AccountPermissionsSynopsisViewImpl(
                        dummy.getId(),
                        dummy.getAccountId(),
                        expectedSpiff,
                        dummy.getExtendedWarranty(),
                        dummy.getStartDate(),
                        dummy.getEndDate()
                );
        
        /*
        assert
         */
        Spiff actualSpiff =
                objectUnderTest
                        .getSpiff();

        assertThat(actualSpiff)
                .isEqualTo(expectedSpiff);

    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullExtendedWarranty_Throws(
    ) throws Exception {

        new AccountPermissionsSynopsisViewImpl(
                dummy.getId(),
                dummy.getAccountId(),
                dummy.getSpiff(),
                null,
                dummy.getStartDate(),
                dummy.getEndDate()
               
        );

    }
    
    @Test
    public void constructor_SetsExtendedWarranty() {
        
        /*
        arrange
         */
    	ExtendedWarranty expectedExtendedWarranty = dummy.getExtendedWarranty();
        
        /*
        act
         */
        AccountPermissionsSynopsisView objectUnderTest =
                new AccountPermissionsSynopsisViewImpl(
                        dummy.getId(),
                        dummy.getAccountId(),
                        dummy.getSpiff(),
                        expectedExtendedWarranty,
                        dummy.getStartDate(),
                        dummy.getEndDate()
                );
        
        /*
        assert
         */
        ExtendedWarranty actualExtendedWarranty =
                objectUnderTest
                        .getExtendedWarranty();

        assertThat(actualExtendedWarranty)
                .isEqualTo(expectedExtendedWarranty);

    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullStartDate_Throws(
    ) throws Exception {

        new AccountPermissionsSynopsisViewImpl(
                dummy.getId(),
                dummy.getAccountId(),
                dummy.getSpiff(),
                dummy.getExtendedWarranty(),
                null,
                dummy.getEndDate()
               
        );

    }
    
    @Test
    public void constructor_SetsStartDate() {
        
        /*
        arrange
         */
    	Instant expectedStartDate = dummy.getStartDate();
        
        /*
        act
         */
        AccountPermissionsSynopsisView objectUnderTest =
                new AccountPermissionsSynopsisViewImpl(
                        dummy.getId(),
                        dummy.getAccountId(),
                        dummy.getSpiff(),
                        dummy.getExtendedWarranty(),
                        expectedStartDate,
                        dummy.getEndDate()
                );
        
        /*
        assert
         */
        Instant actualStartDate =
                objectUnderTest
                        .getStartDate();

        assertThat(actualStartDate)
                .isEqualTo(expectedStartDate);

    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullEndDate_Throws(
    ) throws Exception {

        new AccountPermissionsSynopsisViewImpl(
                dummy.getId(),
                dummy.getAccountId(),
                dummy.getSpiff(),
                dummy.getExtendedWarranty(),
                dummy.getStartDate(),
                null
               
        );

    }
    
    @Test
    public void constructor_SetsEndDate() {
        
        /*
        arrange
         */
    	Instant expectedEndDate = dummy.getEndDate();
        
        /*
        act
         */
        AccountPermissionsSynopsisView objectUnderTest =
                new AccountPermissionsSynopsisViewImpl(
                        dummy.getId(),
                        dummy.getAccountId(),
                        dummy.getSpiff(),
                        dummy.getExtendedWarranty(),
                        dummy.getStartDate(),
                        expectedEndDate
                );
        
        /*
        assert
         */
        Instant actualEndDate =
                objectUnderTest
                        .getEndDate();

        assertThat(actualEndDate)
                .isEqualTo(expectedEndDate);

    }

}
