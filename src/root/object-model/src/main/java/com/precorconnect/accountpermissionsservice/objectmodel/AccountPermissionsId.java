package com.precorconnect.accountpermissionsservice.objectmodel;

public interface AccountPermissionsId {
	
	Integer getValue();

}
