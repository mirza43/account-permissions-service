package com.precorconnect.accountpermissionsservice;

public interface AccountPermissionsServiceConfigFactory {
	
	AccountPermissionsServiceConfig construct();

}
