package com.precorconnect.accountpermissionsservice.objectmodel;

import java.time.Instant;


public interface AccountPermissionsSynopsisView {
	
	AccountPermissionsId getId();
	
	AccountId getAccountId();
	
	Spiff getSpiff();
	
	ExtendedWarranty getExtendedWarranty();
	
	Instant getStartDate();
	
	Instant getEndDate();

}
