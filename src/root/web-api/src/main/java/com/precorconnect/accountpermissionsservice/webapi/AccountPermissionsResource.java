package com.precorconnect.accountpermissionsservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.accountpermissionsservice.AccountPermissionsService;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountIdImpl;
import com.precorconnect.accountpermissionsservice.webapiobjectmodel.AccountPermissionsSynopsisView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/account-permissions")
@Api(value = "/account-permissions", description = "Operations on account permissions")
public class AccountPermissionsResource {
	
	  /*
    fields
     */
	
	private final AccountPermissionsService accountPermissionsService;
	
	private final AccountPermissionsSynopsisViewFactory accountPermissionsSynopsisViewFactory;
	
	private final OAuth2AccessTokenFactory oAuth2AccessTokenFactory;

	@Inject
	public AccountPermissionsResource(
			@NonNull final AccountPermissionsService accountPermissionsService,
			@NonNull final AccountPermissionsSynopsisViewFactory accountPermissionsSynopsisViewFactory,
			@NonNull final OAuth2AccessTokenFactory oAuth2AccessTokenFactory
	 ) {
		
		this.accountPermissionsService = 
				 guardThat(
	                        "accountPermissionsService",
	                        accountPermissionsService
	                		)
	                        .isNotNull()
	                        .thenGetValue();
		
		this.accountPermissionsSynopsisViewFactory = 
				 guardThat(
	                        "accountPermissionsSynopsisViewFactory",
	                        accountPermissionsSynopsisViewFactory
	                		)
	                        .isNotNull()
	                        .thenGetValue();
		
		this.oAuth2AccessTokenFactory = 
				 guardThat(
	                        "oAuth2AccessTokenFactory",
	                        oAuth2AccessTokenFactory
	                		)
	                        .isNotNull()
	                        .thenGetValue();
	}
	
	@RequestMapping(
			 value = "/{accountId}",
			method = RequestMethod.GET
		)
   @ApiOperation(value = "get Account Permissions with Id")
   public AccountPermissionsSynopsisView getAccountPermissionWithId(
   		@PathVariable("accountId") final String accountId,
           @RequestHeader("Authorization") final String authorizationHeader
   ) throws AuthenticationException {

       OAuth2AccessToken accessToken =
               oAuth2AccessTokenFactory
                       .construct(authorizationHeader);
       
     try{
     
       return
    		   accountPermissionsSynopsisViewFactory
       		.construct(
       				accountPermissionsService
                       .getAccountPermissionWithId(
                       		new AccountIdImpl(accountId),
                       		accessToken
                       	)
               );
     }catch(Exception e){
    	 e.printStackTrace();
    	 return null;
     }

   }


}
