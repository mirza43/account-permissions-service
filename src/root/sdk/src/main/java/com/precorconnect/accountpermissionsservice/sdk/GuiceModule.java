package com.precorconnect.accountpermissionsservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.net.URISyntaxException;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.glassfish.jersey.jackson.JacksonFeature;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final AccountPermissionsServiceSdkConfig config;

    /*
    constructors
     */
    @Inject
    public GuiceModule(
            @NonNull final AccountPermissionsServiceSdkConfig config
    ) {

    	this.config =
                guardThat(
                        "config",
                        config
                		)
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure(
    ) {

        bindFeatures();

    }


    private void bindFeatures() {

        bind(GetAccountPermissionsWithAccountIdFeature.class)
                .to(GetAccountPermissionsWithAccountIdFeatureImpl.class);

    }

    @Provides
    @Singleton
    WebTarget constructWebTarget(
            @NonNull final ObjectMapperProvider objectMapperProvider
    ) {

        try {

            return
                    ClientBuilder
                            .newClient()
                            .register(objectMapperProvider)
                            .register(JacksonFeature.class)
                            .target(config.getPrecorConnectApiBaseUrl().toURI());

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

    }

}
