package com.precorconnect.accountpermissionsservice.database;

public interface DatabaseAdapterConfigFactory {

	DatabaseAdapterConfig construct();
}
