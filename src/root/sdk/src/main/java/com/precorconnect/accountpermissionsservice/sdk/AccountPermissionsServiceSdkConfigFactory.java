package com.precorconnect.accountpermissionsservice.sdk;

public interface AccountPermissionsServiceSdkConfigFactory {
	
	AccountPermissionsServiceSdkConfig construct();

}
