package com.precorconnect.accountpermissionsservice.database;

public class ConfigFactory {

	public Config construct(){
		
		return
				new Config(
					new DatabaseAdapterConfigFactoryImpl()
														.construct()
                );
	}		   
}
