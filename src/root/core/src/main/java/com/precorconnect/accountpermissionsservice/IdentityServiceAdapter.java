package com.precorconnect.accountpermissionsservice;

import com.precorconnect.AccessContext;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.AuthenticationException;
import org.checkerframework.checker.nullness.qual.NonNull;

public interface IdentityServiceAdapter {

    AccessContext getAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;

}
