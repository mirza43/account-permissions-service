package com.precorconnect.accountpermissionsservice.database;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.flywaydb.core.Flyway;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.accountpermissionsservice.DatabaseAdapter;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;

public class DatabaseAdapterImpl implements DatabaseAdapter {
	
	 /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public DatabaseAdapterImpl(
            @NonNull final DatabaseAdapterConfig config
    ) {

        // ensure database schema up to date
        Flyway flyway = new Flyway();
        flyway.setDataSource(
	                config.getUri().toString(),
	                config.getUsername().getValue(),
	                config.getPassword().getValue()
                );
        flyway.migrate();

        GuiceModule guiceModule =
                new GuiceModule(
                        config
                );

        injector =
                Guice.createInjector(guiceModule);

    }

	@Override
	public AccountPermissionsSynopsisView getAccountPermissionWithId(
			@NonNull AccountId accountId
			) {

		
		return
				injector.getInstance(GetAccountPermissionsWithIdFeature.class)
				.execute(accountId);
	}

	
}
