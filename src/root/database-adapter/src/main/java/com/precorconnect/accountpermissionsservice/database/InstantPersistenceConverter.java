package com.precorconnect.accountpermissionsservice.database;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Calendar;

import javax.persistence.AttributeConverter;

public class InstantPersistenceConverter
	implements AttributeConverter<java.time.Instant, java.sql.Timestamp> {

    @Override
	public Timestamp convertToDatabaseColumn(
            final Instant instant
    ) {

        long actualEpochMilli =
                instant.toEpochMilli();

        // we need to subtract what jdbc automatically adds
        long jdbcConversionCompensatedEpochMilli =
                actualEpochMilli - getCurrentRawOffsetMilli();

        return
        		new
        			java.sql.Timestamp(
        					jdbcConversionCompensatedEpochMilli
        			);
    }

    @Override
	public Instant convertToEntityAttribute(
            final Timestamp timestamp
    ) {

        long jdbcConversionCompensatedEpochMilli =
                timestamp.getTime();

        // we need to add what jdbc automatically subtracts
        long actualEpochMilli =
                jdbcConversionCompensatedEpochMilli + getCurrentRawOffsetMilli();

        return
        		Instant
        				.ofEpochMilli(
        						actualEpochMilli
        						);
    }

    private long getCurrentRawOffsetMilli() {

        return
        		Calendar
        				.getInstance()
        				.getTimeZone()
        				.getRawOffset();

    }
}
