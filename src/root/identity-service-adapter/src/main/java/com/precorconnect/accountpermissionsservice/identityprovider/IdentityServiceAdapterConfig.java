package com.precorconnect.accountpermissionsservice.identityprovider;

import java.net.URL;

public interface IdentityServiceAdapterConfig {

    URL getPrecorConnectApiBaseUrl();

}
