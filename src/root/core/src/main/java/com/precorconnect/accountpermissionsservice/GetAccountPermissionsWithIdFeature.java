package com.precorconnect.accountpermissionsservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccessContext;
import com.precorconnect.AuthenticationException;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;

public interface GetAccountPermissionsWithIdFeature {

	AccountPermissionsSynopsisView execute(
			@NonNull AccountId accountId,
			@NonNull AccessContext accessContext
			)throws AuthenticationException;
}
