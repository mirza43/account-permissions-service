package com.precorconnect.accountpermissionsservice.sdk;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.webapiobjectmodel.AccountPermissionsSynopsisView;

public class AccountPermissionsServiceSdkImpl implements AccountPermissionsServiceSdk {
	
	 /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public AccountPermissionsServiceSdkImpl(
            @NonNull AccountPermissionsServiceSdkConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(
                        config
                );

        injector =
                Guice.createInjector(guiceModule);
    }


	@Override
	public AccountPermissionsSynopsisView getAccountPermissionWithId(@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken oAuth2AccessToken) throws AuthenticationException {

		return 
				injector.getInstance(GetAccountPermissionsWithAccountIdFeature.class)
				.execute(accountId, oAuth2AccessToken);
	}

}
