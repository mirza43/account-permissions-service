package com.precorconnect.accountpermissionsservice.database;

import org.junit.Test;

import com.precorconnect.accountpermissionsservice.DatabaseAdapter;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisViewImpl;

import static org.assertj.core.api.Assertions.assertThat;

public class DatabaseAdapterImplIT {

    /*
    fields
     */
    private final Config config = 
    								new ConfigFactory()
    								                   .construct();
    
    private final Dummy dummy=
    							new Dummy();

    /*
    test methods
     */
    @Test
    public void listCustomerSources_ReturnsMoreThanOneResult(
    ) throws Exception {

        /*
        arrange
         */
        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        /*
        act
         */
       AccountPermissionsSynopsisView
                discountCodesSynopsisView =
                objectUnderTest.getAccountPermissionWithId(
                								dummy.getAccountId()
                							);
       
       
       AccountPermissionsSynopsisView expectedObject=
    		   new AccountPermissionsSynopsisViewImpl(
    				   dummy.getId(),
    				   dummy.getAccountId(),
    				   dummy.getSpiff(),
    				   dummy.getExtendedWarranty(),
    				   dummy.getStartDate(),
    				   dummy.getEndDate()
    				   );
        /*
        assert
         */
        assertThat(discountCodesSynopsisView)
                .isEqualTo(expectedObject);
    }

}
