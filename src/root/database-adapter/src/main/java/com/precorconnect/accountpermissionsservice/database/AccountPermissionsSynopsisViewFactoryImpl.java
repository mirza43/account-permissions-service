package com.precorconnect.accountpermissionsservice.database;

import java.time.Instant;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Singleton;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountIdImpl;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsIdImpl;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisViewImpl;
import com.precorconnect.accountpermissionsservice.objectmodel.ExtendedWarranty;
import com.precorconnect.accountpermissionsservice.objectmodel.ExtendedWarrantyImpl;
import com.precorconnect.accountpermissionsservice.objectmodel.Spiff;
import com.precorconnect.accountpermissionsservice.objectmodel.SpiffImpl;

@Singleton
public class AccountPermissionsSynopsisViewFactoryImpl implements AccountPermissionsSynopsisViewFactory {

	@Override
	public AccountPermissionsSynopsisView construct(
			@NonNull AccountPermissions accountPermissions
		) {
		
		AccountPermissionsId id=
				new AccountPermissionsIdImpl(accountPermissions.getId());
		
		AccountId accountId=
				new AccountIdImpl(accountPermissions.getAccountId());
		
		Spiff spiff=
				new SpiffImpl(accountPermissions.isSpiff());
		
		ExtendedWarranty extendedWarranty=
				new ExtendedWarrantyImpl(accountPermissions.isExtendedWarranty());
		
		Instant startDate=
				accountPermissions.getStartDate();
		
		Instant endDate=
				accountPermissions.getEndDate();
		
		return 
				new AccountPermissionsSynopsisViewImpl(
						id,
						accountId,
						spiff,
						extendedWarranty,
						startDate,
						endDate
						);
	}

}
