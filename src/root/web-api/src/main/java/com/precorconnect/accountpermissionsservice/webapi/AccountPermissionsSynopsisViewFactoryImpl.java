package com.precorconnect.accountpermissionsservice.webapi;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.accountpermissionsservice.webapiobjectmodel.AccountPermissionsSynopsisView;

@Component
public class AccountPermissionsSynopsisViewFactoryImpl implements AccountPermissionsSynopsisViewFactory {

	@Override
	public AccountPermissionsSynopsisView construct(
			com.precorconnect.accountpermissionsservice.objectmodel.@NonNull AccountPermissionsSynopsisView accountPermissionsSynopsisView
			) {
		
		return 
				
				new AccountPermissionsSynopsisView(
						
						accountPermissionsSynopsisView
												.getId()
												 .getValue(),
												 
						accountPermissionsSynopsisView
												.getAccountId()
												   .getValue(),
												   
						accountPermissionsSynopsisView
						                           .getSpiff()
						                              .getValue(),
						                              
						accountPermissionsSynopsisView
						                          .getExtendedWarranty()
						                               .getValue(),
						                               
						new SimpleDateFormat("MM/dd/yyyy").format(
								 Date.from(accountPermissionsSynopsisView
											.getStartDate())
								 ), 
						
						new SimpleDateFormat("MM/dd/yyyy").format(
								 Date.from(accountPermissionsSynopsisView
											.getEndDate())
								 )
						);
	}

}
