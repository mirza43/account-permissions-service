package com.precorconnect.accountpermissionsservice.webapi;

import com.precorconnect.identityservice.HmacKey;
import com.precorconnect.identityservice.HmacKeyImpl;

public final class ConfigFactory {

	public Config construct() {

		String hamckey = System
				             .getenv("TEST_IDENTITY_SERVICE_JWT_SIGNING_KEY");

		HmacKey hamcKey =
				new HmacKeyImpl(
						hamckey
				);

		String precorConnectApiBaseUrl = System
				             .getenv("PRECOR_CONNECT_API_BASE_URL");

		return
				new Config(
						hamcKey,
						precorConnectApiBaseUrl
				);

	}

}
