package com.precorconnect.accountpermissionsservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.identityservice.HmacKey;

public class Config {

    /*
    fields
     */
    private final HmacKey identityServiceJwtSigningKey;

    private final String precorConnectApiBaseUrl;

    /*
    constructors
    */
    public Config(
		@NonNull final HmacKey identityServiceJwtSigningKey,
		@NonNull final String precorConnectApiBaseUrl
    ) {

    	this.identityServiceJwtSigningKey = identityServiceJwtSigningKey;

    	this.precorConnectApiBaseUrl = precorConnectApiBaseUrl;
    	
    }

    /*
    getter methods
     */
    public HmacKey getIdentityServiceJwtSigningKey() {
        return identityServiceJwtSigningKey;
    }

	public String getPrecorConnectApiBaseUrl() {
		return precorConnectApiBaseUrl;
	}



}
