package com.precorconnect.accountpermissionsservice.webapi;

import javax.inject.Singleton;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.precorconnect.accountpermissionsservice.AccountPermissionsService;
import com.precorconnect.accountpermissionsservice.AccountPermissionsServiceConfigFactoryImpl;
import com.precorconnect.accountpermissionsservice.AccountPermissionsServiceImpl;


@Configuration
public class AccountPermissionsServiceBean {
	
	 @Bean
		@Singleton
		public AccountPermissionsService discountCodesService(){
			 return 
					 new AccountPermissionsServiceImpl(
							 new AccountPermissionsServiceConfigFactoryImpl().construct()
							 );
		 }

}
