package com.precorconnect.accountpermissionsservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccessContext;
import com.precorconnect.AuthenticationException;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;

@Singleton
public class GetAccountPermissionsWithIdFeatureImpl implements GetAccountPermissionsWithIdFeature {
	
	 /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public GetAccountPermissionsWithIdFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                        databaseAdapter
                		)
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public AccountPermissionsSynopsisView execute(
			@NonNull AccountId accountId,
			@NonNull AccessContext accessContext
			)throws AuthenticationException {
		
		assertAuthorized(accessContext);
		

		return 
				databaseAdapter.getAccountPermissionWithId(accountId);
				
	}
	
	private void assertAuthorized(
            @NonNull final AccessContext accessContext
    ) throws AuthenticationException {

        // an access context is required to get a customer brand
    	guardThat(
                "accessContext",
                accessContext
    			)
                .isNotNull()
                .thenGetValue();

    }

}
