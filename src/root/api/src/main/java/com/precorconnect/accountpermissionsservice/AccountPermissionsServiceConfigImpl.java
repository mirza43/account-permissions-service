package com.precorconnect.accountpermissionsservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.accountpermissionsservice.database.DatabaseAdapterConfig;
import com.precorconnect.accountpermissionsservice.identityprovider.IdentityServiceAdapterConfig;

public class AccountPermissionsServiceConfigImpl implements AccountPermissionsServiceConfig {
	
	 /*
    fields
     */
    private final DatabaseAdapterConfig databaseAdapterConfig;

    private final IdentityServiceAdapterConfig identityServiceAdapterConfig;

    /*
    constructors
     */
    public AccountPermissionsServiceConfigImpl(
            @NonNull final DatabaseAdapterConfig databaseAdapterConfig,
            @NonNull final IdentityServiceAdapterConfig identityServiceAdapterConfig
    ) {

    	this.databaseAdapterConfig =
                guardThat(
                        "databaseAdapterConfig",
                        databaseAdapterConfig
                		)
                        .isNotNull()
                        .thenGetValue();

    	this.identityServiceAdapterConfig =
                guardThat(
                        "identityServiceAdapterConfig",
                        identityServiceAdapterConfig
                		)
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
    */
	@Override
	public DatabaseAdapterConfig getDatabaseAdapterConfig() {
		return databaseAdapterConfig;
	}

	@Override
	public IdentityServiceAdapterConfig getIdentityServiceAdapterConfig() {
		return identityServiceAdapterConfig;
	}

	 /*
    equality methods
     */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((databaseAdapterConfig == null) ? 0 : databaseAdapterConfig.hashCode());
		result = prime * result
				+ ((identityServiceAdapterConfig == null) ? 0 : identityServiceAdapterConfig.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountPermissionsServiceConfigImpl other = (AccountPermissionsServiceConfigImpl) obj;
		if (databaseAdapterConfig == null) {
			if (other.databaseAdapterConfig != null)
				return false;
		} else if (!databaseAdapterConfig.equals(other.databaseAdapterConfig))
			return false;
		if (identityServiceAdapterConfig == null) {
			if (other.identityServiceAdapterConfig != null)
				return false;
		} else if (!identityServiceAdapterConfig.equals(other.identityServiceAdapterConfig))
			return false;
		return true;
	}
	
	

}
