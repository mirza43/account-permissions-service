package com.precorconnect.accountpermissionsservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;


import org.checkerframework.checker.nullness.qual.NonNull;

public class AccountPermissionsSynopsisView {
	
	 /*
    fields
     */
    private final Integer id;

    private final String accountId;
    
    private final boolean spiff;
    
    private final boolean extendedWarranty;
    
    private final String startDate;
    
    private final String endDate;

    /*
    constructors
     */
	public AccountPermissionsSynopsisView(
			@NonNull Integer id,
			@NonNull String accountId,
			@NonNull boolean spiff, 
			@NonNull boolean extendedWarranty,
			@NonNull String startDate,
			@NonNull String endDate
		) {
		
		this.id = 
				guardThat(
                        "id",
                        id
                		)
                        .isNotNull()
                        .thenGetValue();
		
		this.accountId =
				guardThat(
                        "accountId",
                        accountId
                		)
                        .isNotNull()
                        .thenGetValue();
		
		this.spiff = 
				guardThat(
                        "spiff",
                        spiff
                		)
                        .isNotNull()
                        .thenGetValue();
		
		this.extendedWarranty =
				guardThat(
                        "extendedWarranty",
                        extendedWarranty
                		)
                        .isNotNull()
                        .thenGetValue();
		
		this.startDate =
				guardThat(
                        "startDate",
                        startDate
                		)
                        .isNotNull()
                        .thenGetValue();
		
		this.endDate =
				
				guardThat(
                        "endDate",
                        endDate
                		)
                        .isNotNull()
                        .thenGetValue();
	}
	
	/**
	 * 
	 * getter methods
	 */

	public Integer getId() {
		return id;
	}

	public String getAccountId() {
		return accountId;
	}

	public boolean isSpiff() {
		return spiff;
	}

	public boolean isExtendedWarranty() {
		return extendedWarranty;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	/*
    equality methods
     */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + (extendedWarranty ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (spiff ? 1231 : 1237);
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountPermissionsSynopsisView other = (AccountPermissionsSynopsisView) obj;
		if (accountId == null) {
			if (other.accountId != null)
				return false;
		} else if (!accountId.equals(other.accountId))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} 
		if (extendedWarranty != other.extendedWarranty)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (spiff != other.spiff)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} 
		return true;
	}
    
   

	
    
    
}
