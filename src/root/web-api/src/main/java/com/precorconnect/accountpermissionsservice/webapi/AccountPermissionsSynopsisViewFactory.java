package com.precorconnect.accountpermissionsservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.accountpermissionsservice.webapiobjectmodel.AccountPermissionsSynopsisView;

public interface AccountPermissionsSynopsisViewFactory {
	
	AccountPermissionsSynopsisView construct(
			 com.precorconnect.accountpermissionsservice.objectmodel.
			 @NonNull AccountPermissionsSynopsisView accountPermissionsSynopsisView
		);

}
