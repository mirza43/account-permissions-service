package com.precorconnect.accountpermissionsservice.database;

import static org.assertj.core.api.StrictAssertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Test;

import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisViewImpl;


public class AccountPermissionsSynopsisViewFactoryTest {
	
	private AccountPermissionsSynopsisViewFactory accountPermissionsSynopsisViewFactory;
	
	private final Dummy dummy = new Dummy();	
	
	
	@Before
	public void setUp(){
		accountPermissionsSynopsisViewFactory = new AccountPermissionsSynopsisViewFactoryImpl();
	}
	
	@Test
	public void test_construct_passing_mandatory_fields(){
		
		
	AccountPermissionsSynopsisView accountPermissionsSynopsisView =	accountPermissionsSynopsisViewFactory.construct(construct_entity_object());
	
	AccountPermissionsSynopsisView expectedAccountPermissionsObject=construct_expected_accountPermissionsSynopsisViewObj();
	
	assertThat(accountPermissionsSynopsisView).isEqualTo(expectedAccountPermissionsObject);
	
	}

	
	public AccountPermissions construct_entity_object(){
		AccountPermissions accountPermissions=new AccountPermissions();
		try{
		accountPermissions.setId(1);
		accountPermissions.setAccountId("161018");
		accountPermissions.setSpiff(true);
		accountPermissions.setExtendedWarranty(true);
		accountPermissions.setStartDate(
										new SimpleDateFormat("MM/dd/yyyy")
										.parse("1/1/2016")
										.toInstant()
								);
		accountPermissions.setEndDate(
				new SimpleDateFormat("MM/dd/yyyy")
				.parse("12/31/2099")
				.toInstant()
				);
		
		}catch(ParseException e){
			 throw new RuntimeException(e);
		}
		
		return accountPermissions;
	}
	
	public AccountPermissionsSynopsisView construct_expected_accountPermissionsSynopsisViewObj(){
		
		return 
				new AccountPermissionsSynopsisViewImpl(dummy.getId(),
						dummy.getAccountId(),
						dummy.getSpiff(),
						dummy.getExtendedWarranty(),
						dummy.getStartDate(),
						dummy.getEndDate()
				);
	}
	
	
}
