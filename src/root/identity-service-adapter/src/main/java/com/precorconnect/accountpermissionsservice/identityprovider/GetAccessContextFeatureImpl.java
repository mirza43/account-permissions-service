package com.precorconnect.accountpermissionsservice.identityprovider;

import static com.precorconnect.guardclauses.Guards.guardThat;

import com.precorconnect.AccessContext;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.identityservice.sdk.IdentityServiceSdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class GetAccessContextFeatureImpl
        implements GetAccessContextFeature {

    /*
    fields
     */
    private final IdentityServiceSdk identityServiceSdk;

    /*
    constructors
     */
    @Inject
    public GetAccessContextFeatureImpl(
            @NonNull IdentityServiceSdk identityServiceSdk
    ) {

        this.identityServiceSdk = 
				guardThat(
						"identityServiceSdk",
						identityServiceSdk
				)
						.isNotNull()
						.thenGetValue();

    }

    @Override
    public AccessContext execute(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return identityServiceSdk
                .getAccessContext(
                        accessToken
                );

    }
}
