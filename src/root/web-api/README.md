## Description
Precor Connect Account Permissions API for Rest.

## Features

##### Get Account Permissions With Account Id 
* [documentation](features/accountPermissions.feature)

## API Explorer

##### Environments:
-  [dev](https://dev.precorconnect.com/)
-  [prod](https://precorconnect.com/)