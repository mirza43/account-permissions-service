package com.precorconnect.accountpermissionsservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

public class Config {

    /*
    fields
     */
    private final DatabaseAdapterConfig databaseAdapterConfig;

    /*
    constructors
     */
    public Config(
    		@NonNull final DatabaseAdapterConfig databaseAdapterConfig
    ) {

            this.databaseAdapterConfig = databaseAdapterConfig;

     }

    /*
    getter methods
    */
    public DatabaseAdapterConfig getDatabaseAdapterConfig() {

        return databaseAdapterConfig;

    }
}
