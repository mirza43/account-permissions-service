package com.precorconnect.accountpermissionsservice;


import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.precorconnect.AccessContext;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;


class CoreImpl 
	implements Core {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public CoreImpl(
            @NonNull final DatabaseAdapter databaseAdapter,
            @NonNull final IdentityServiceAdapter identityServiceAdapter
    ) {

        GuiceModule guiceModule =
                new GuiceModule(
                        databaseAdapter,
                        identityServiceAdapter
                        );

        injector = Guice.createInjector(guiceModule);
    }

	@Override
	public AccountPermissionsSynopsisView getAccountPermissionWithId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException {
		
		AccessContext accessContext =
                injector.getInstance(IdentityServiceAdapter.class)
                        .getAccessContext(oAuth2AccessToken);
		
		
		return	
				injector
				        .getInstance(GetAccountPermissionsWithIdFeature.class)
				           .execute(accountId, accessContext);
					
	}

	

    

}
