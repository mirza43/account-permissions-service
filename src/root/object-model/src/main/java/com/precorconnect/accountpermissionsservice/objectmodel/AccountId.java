package com.precorconnect.accountpermissionsservice.objectmodel;

public interface AccountId {
	
	String getValue();
}
