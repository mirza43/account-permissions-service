package com.precorconnect.accountpermissionsservice.webapi;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.RestAssured;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({"server.port=0"})
public class AccountPermissionsResourceIT {
	
	/*
    fields
     */
	private final Config config=
						 new ConfigFactory()
    									 .construct();
	
	private final Dummy dummy=
							new Dummy();
	
    private final Factory factory =
            new Factory(
                    new Dummy(),
                    new IdentityServiceIntegrationTestSdkImpl(
                    			config
                                      .getIdentityServiceJwtSigningKey()
                    )
            );
    
    @Value("${local.server.port}")
    int port;


    /*
    steps
     */

    @Before
    public void setUp() {

        RestAssured.port = port;
    }
    
    @Test
  public void getAccountPermissions_With_AccountId_ReturnsAccountPermissionsSynopsisViewObj() {
    	

        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                factory
                                        .constructValidAppOAuth2AccessToken()
                                        .getValue()
                        )
                )
                .get("/account-permissions/"+dummy.getAccountId().getValue())
                .then()
                .assertThat()
                .statusCode(200);

    }
}
