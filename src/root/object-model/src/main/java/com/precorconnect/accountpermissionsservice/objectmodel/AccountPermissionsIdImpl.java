package com.precorconnect.accountpermissionsservice.objectmodel;

import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

public class AccountPermissionsIdImpl implements AccountPermissionsId {
	
	/*
	 *  fields
     */
    private final Integer value;

    /*
    constructor methods
     */
    public AccountPermissionsIdImpl(
    		@NonNull Integer value
    		){

    	this.value = 
    			 guardThat(
                         "Id",
                         value
                 )
                         .isNotNull()
                         .thenGetValue();

    }

	@Override
	public Integer getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountPermissionsIdImpl other = (AccountPermissionsIdImpl) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	

}
