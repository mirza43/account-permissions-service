package com.precorconnect.accountpermissionsservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;


public interface AccountPermissionsService {
	
	AccountPermissionsSynopsisView getAccountPermissionWithId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			)throws AuthenticationException;

}
