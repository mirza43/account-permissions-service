package com.precorconnect.accountpermissionsservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.identityservice.HmacKey;

@Component
public class Config {

    /*
    fields
     */
    private HmacKey identityServiceJwtSigningKey;

    /*
    constructors
     */
  	public Config(
			@NonNull final HmacKey identityServiceJwtSigningKey) {

		this.identityServiceJwtSigningKey = identityServiceJwtSigningKey;

	}

    /*
    getter methods
     */
    public HmacKey getIdentityServiceJwtSigningKey() {
        return identityServiceJwtSigningKey;
    }

}
