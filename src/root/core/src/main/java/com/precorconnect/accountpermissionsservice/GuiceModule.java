package com.precorconnect.accountpermissionsservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;


class GuiceModule extends
        AbstractModule {

	 /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;
    
    private final IdentityServiceAdapter identityServiceAdapter;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull final DatabaseAdapter databaseAdapter,
            @NonNull final IdentityServiceAdapter identityServiceAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                        databaseAdapter
                		)
                        .isNotNull()
                        .thenGetValue();

    	this.identityServiceAdapter =
                guardThat(
                        "identityServiceAdapter",
                        identityServiceAdapter
                		)
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindAdapters();
        bindFeatures();

    }

    private void bindAdapters() {

        bind(DatabaseAdapter.class)
                .toInstance(databaseAdapter);

        bind(IdentityServiceAdapter.class)
                .toInstance(identityServiceAdapter);

    }

    private void bindFeatures() {

    	 bind(GetAccountPermissionsWithIdFeature.class)
         .to(GetAccountPermissionsWithIdFeatureImpl.class);
     

    }
}
