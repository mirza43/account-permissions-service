package com.precorconnect.accountpermissionsservice.identityprovider;

public interface IdentityServiceAdapterConfigFactory {
	
	IdentityServiceAdapterConfig construct();
	
}
