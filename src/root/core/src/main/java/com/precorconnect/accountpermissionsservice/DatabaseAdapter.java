package com.precorconnect.accountpermissionsservice;



import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.accountpermissionsservice.objectmodel.AccountId;
import com.precorconnect.accountpermissionsservice.objectmodel.AccountPermissionsSynopsisView;


public interface DatabaseAdapter {

    AccountPermissionsSynopsisView getAccountPermissionWithId(
    		@NonNull AccountId accountId
    		);

}
