package com.precorconnect.accountpermissionsservice.webapi;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features={"features/accountPermissions.feature"},
		glue={"com.precorconnect.accountpermissionsservice.webapi.accountpermissionssynopsisview"}
		)
public class GetAccountPermissionsWithAccountIdFeatureIT {
	
	

}
