package com.precorconnect.accountpermissionsservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.time.Instant;

import org.checkerframework.checker.nullness.qual.NonNull;


public class AccountPermissionsSynopsisViewImpl implements AccountPermissionsSynopsisView {
	
	/*
    fields
    */
	
	private AccountPermissionsId id;;
	
	private AccountId accountId;
	
	private Spiff spiff;
	
	private ExtendedWarranty extendedWarranty;
	
	private Instant startDate;
	
	private Instant endDate;
	
	/*
    constructors
    */
	
	public AccountPermissionsSynopsisViewImpl(
			@NonNull AccountPermissionsId id, 
			@NonNull AccountId accountId, 
			@NonNull Spiff spiff,
			@NonNull ExtendedWarranty extendedWarranty,
			@NonNull Instant startDate, 
			@NonNull Instant endDate
	) {
		
		this.id = 
				guardThat(
                        "AccountPermissionsId",
                        id
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.accountId = 
				guardThat(
                        "accountId",
                        accountId
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.spiff = 
				
				guardThat(
                        "spiff",
                        spiff
                )
                        .isNotNull()
                        .thenGetValue();
		
		
		this.extendedWarranty = 
				guardThat(
                        "extendedWarranty",
                        extendedWarranty
                )
                        .isNotNull()
                        .thenGetValue();
				
		this.startDate = 
				guardThat(
                        "startDate",
                        startDate
                )
                        .isNotNull()
                        .thenGetValue();
		this.endDate = 
				
				guardThat(
                        "endDate",
                        endDate
                )
                        .isNotNull()
                        .thenGetValue();
	}

	@Override
	public AccountPermissionsId getId() {
		return id;
	}

	@Override
	public AccountId getAccountId() {
		return accountId;
	}

	@Override
	public Spiff getSpiff() {
		return spiff;
	}

	@Override
	public ExtendedWarranty getExtendedWarranty() {
		return extendedWarranty;
	}

	@Override
	public Instant getStartDate() {
		return startDate;
	}

	@Override
	public Instant getEndDate() {
		return endDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((extendedWarranty == null) ? 0 : extendedWarranty.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((spiff == null) ? 0 : spiff.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountPermissionsSynopsisViewImpl other = (AccountPermissionsSynopsisViewImpl) obj;
		if (accountId == null) {
			if (other.accountId != null)
				return false;
		} else if (!accountId.equals(other.accountId))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} 
		if (extendedWarranty == null) {
			if (other.extendedWarranty != null)
				return false;
		} else if (!extendedWarranty.equals(other.extendedWarranty))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (spiff == null) {
			if (other.spiff != null)
				return false;
		} else if (!spiff.equals(other.spiff))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} 
		return true;
	}

}
