create table AccountPermissions(
Id                     BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL,
AccountId       CHAR(18)                          NOT NULL,
Spiff BOOLEAN DEFAULT FALSE,
ExtendedWarranty   BOOLEAN DEFAULT FALSE,
StartDate   DATE NOT NULL,
EndDate DATE NOT NULL
)
 SELECT
      1                        AS Id,
      '161018' AS AccountId,
     TRUE As Spiff,
     TRUE As ExtendedWarranty,
     '2016-01-01' AS StartDate,
     '2099-12-31' AS EndDate
       
    UNION
    SELECT
      2                                AS Id,
     '203737' AS AccountId,
     TRUE As Spiff,
     FALSE As ExtendedWarranty,
     '2016-01-01' AS StartDate,
     '2099-12-31' AS EndDate
      
    UNION
    SELECT
      3                      AS Id,
     '156940' AS AccountId,
     FALSE As Spiff,
     TRUE As ExtendedWarranty,
     '2016-01-01' AS StartDate,
     '2099-12-31' AS EndDate
      
    UNION
    SELECT
      4                  AS Id,
     '286461' AS AccountId,
     TRUE As Spiff,
     FALSE As ExtendedWarranty,
     '2016-01-01' AS StartDate,
     '2099-12-31' AS EndDate
     
    UNION
    SELECT
     5                  AS Id,
     '161087' AS AccountId,
     TRUE As Spiff,
     TRUE As ExtendedWarranty,
     '2016-01-01' AS StartDate,
     '2016-3-31' AS EndDate
   