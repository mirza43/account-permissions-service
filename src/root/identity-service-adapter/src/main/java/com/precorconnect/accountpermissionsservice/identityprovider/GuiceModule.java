package com.precorconnect.accountpermissionsservice.identityprovider;

import static com.precorconnect.guardclauses.Guards.guardThat;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.precorconnect.identityservice.sdk.IdentityServiceSdk;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkConfigImpl;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkImpl;

import org.checkerframework.checker.nullness.qual.NonNull;

import javax.inject.Singleton;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final IdentityServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull IdentityServiceAdapterConfig config
    ) {

        this.config = 
				guardThat(
						"config",
						config
				)
						.isNotNull()
						.thenGetValue();

    }

    @Override
    protected void configure() {

        bindFeatures();

    }

    private void bindFeatures() {

        bind(GetAccessContextFeature.class)
                .to(GetAccessContextFeatureImpl.class)
                .in(Singleton.class);

    }

    @Provides
    @Singleton
    IdentityServiceSdk identityServiceSdk() {

        return
                new IdentityServiceSdkImpl(
                        new IdentityServiceSdkConfigImpl(
                                config.getPrecorConnectApiBaseUrl()
                        )
                );

    }

}
