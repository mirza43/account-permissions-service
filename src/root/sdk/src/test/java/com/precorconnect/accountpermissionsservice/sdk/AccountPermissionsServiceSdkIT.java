package com.precorconnect.accountpermissionsservice.sdk;

import static org.assertj.core.api.StrictAssertions.assertThat;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.precorconnect.accountpermissionsservice.webapi.Application;
import com.precorconnect.accountpermissionsservice.webapiobjectmodel.AccountPermissionsSynopsisView;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({"server.port=0"})
public class AccountPermissionsServiceSdkIT {
	
	/*
    fields
     */
    @Value("${local.server.port}")
    int port;
    
    private final Config config=
    							 new ConfigFactory().construct();
    
    private final Dummy dummy=
    		             new Dummy();
    
    private final Factory factory =
            new Factory(
                    new IdentityServiceIntegrationTestSdkImpl(
                    		config.getIdentityServiceJwtSigningKey()
                    ),
                    dummy
            );

    /*
    test methods
     */
    
    @Test
    public void getDiscountCodes_With_Code_Return_DiscountCodeSynopsisViewObj(
    ) throws Exception {

        /*
        arrange
         */
    	AccountPermissionsServiceSdk objectUnderTest =
                new AccountPermissionsServiceSdkImpl(
                        new AccountPermissionsServiceSdkConfigFactoryImpl()
                        										.construct()
                );
        /*
        act
         */
    	AccountPermissionsSynopsisView
                accountPermissionsSynopsisView =
                objectUnderTest.getAccountPermissionWithId(
                		dummy.getAccountId(),
                        factory.constructValidAppOAuth2AccessToken()
                );
        
    	AccountPermissionsSynopsisView expectedObject=
     		   new AccountPermissionsSynopsisView(
     				   dummy.getId().getValue(),
     				   dummy.getAccountId().getValue(),
     				   dummy.getSpiff().getValue(),
     				   dummy.getExtendedWarranty().getValue(),
     				  new SimpleDateFormat("MM/dd/yyyy").format(
								 Date.from(dummy.getStartDate())
								 ),
						
     		   				new SimpleDateFormat("MM/dd/yyyy").format(
						 Date.from(dummy.getStartDate())
						 )
				
     				   );

        /*
        assert
         */
        assertThat(accountPermissionsSynopsisView)
                .isEqualTo(expectedObject);

    }

}
